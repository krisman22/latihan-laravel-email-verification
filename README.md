<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Laravel Email Verification dan Send Email

Laravel menyediakan fitur verifikasi email dan pengiriman email dengan mudah.

## Verifikasi Email

Sebelum melakukan verifikasi email kita wajib menyediakan server smtp, yang akan melakukan pengiriman email.

### Settingan SMTP pada .env
Setelah mendapatkan SMTP server, silahkan isi informasi seperti di bawah ini. Pada contoh ini kita menggunakan mailtrap untuk melakukan uji coba.

```
MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=b28af25442d09c
MAIL_PASSWORD=3676cc67fcd616
MAIL_ENCRYPTION=tls
```
### Settingan Route web.php

Menambahkan beberapa route untuk email verification.

Tambahkan pada baris route.
```
Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
 
    return redirect('/dashboard');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
 
    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');
```

Tambahkan pada baris use :
```
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

```
### Settingan pada app/Models/User.php
tambahkan `MustVerifyEmail` seperti di bawah ini :

```
<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];
}

```


### Pengaturan Tambahan
Buka `vendor\swiftmailer\lib\classes\Swift\Transport\StreamBuffer.php` dan jadikan komentar kode `$options = [];`
Kemudian paste-kan kode berikut dibawahnya `$options['ssl'] = array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true);` .

Jalankan perintah `php artisan config:clear`.

### Templating Blade Email Verifikasi
Lakukan settingan pada `app/Providers/AuthServiceProvider.php` 

tambahkan seperti dibawah ini :
```
public function boot()
    {
        $this->registerPolicies();

        //
        VerifyEmail::toMailUsing(function ($notifiable, $url) {
            $mail = new MailMessage;
            $mail->subject('Verifikasi Email');
            $mail->markdown('mail.emailVerify', ['url' => $url]);
            return $mail;
        });
    }
```

tambahkan pada baris use :
```
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\URL;
```

Sekarang kita akan membuat file blade nya pada view `mail.emailVerify`. Pada email blade ini kita bisa melakukan custom HTML sesuai yang kita inginkan. Untuk melihat file contohnya silahkan buka `resources/views/mail/emailVerify.blade.php` .


