@component('mail::message')
    Terimakasih telah mendaftar di Hicoko Studio.


@component('mail::button', ['url' => $url])
Verify Email
@endcomponent



Regards,<br>
Krisman Zebua <br>
Direktur
@endcomponent